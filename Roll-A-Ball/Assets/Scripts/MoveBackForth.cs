using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackForth : MonoBehaviour
{
    public Transform start;
    public Transform end;

    public float duration;
    public float startWait;
    public float endWait;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = calculatePos(Time.time);
    }

    Vector3 calculatePos(float time)
    {
        float modTime = time % (startWait + duration + endWait + duration);
        if (modTime < startWait) {
            return start.position;
        } else if (modTime < startWait + duration) {
            return Vector3.Lerp(start.position, end.position, (modTime - startWait) / duration);
        } else if (modTime < startWait + duration + endWait) {
            return end.position;
        } else {
            return Vector3.Lerp(end.position, start.position, (modTime - startWait - duration - endWait) / duration);
        }
    }
}
