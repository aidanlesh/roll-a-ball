using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour
{

    public float bump_force;
    public GameObject myModel;

    private float bump_timer = float.PositiveInfinity;
    public float anim_duration = 0.2f;
    public float anim_scale = 1.2f;

    public bool bumped_yet = false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float model_scale = Mathf.Lerp(anim_scale, 1f, Mathf.Max(bump_timer / anim_duration, 0f));
        myModel.transform.localScale = new Vector3(model_scale, model_scale, model_scale) ;
        bump_timer += Time.deltaTime;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.rigidbody) {
            other.rigidbody.AddForce(other.GetContact(0).normal * bump_force, ForceMode.Impulse);
            bump_timer = 0;
            if (!bumped_yet && other.gameObject.GetComponent<PlayerController>()) {
                other.gameObject.GetComponent<PlayerController>().IncreaseScore(20);
                bumped_yet = true;
            }
        }
    }
}
