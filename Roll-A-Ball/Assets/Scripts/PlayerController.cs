using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float floor_speed = 0;
    public float air_speed = 0;
    private float speed;
    public int score = 0;
    
    private Rigidbody rb;
    private Collider cl;
    private float movementX;
    private float movementY;

    public float neutral_drag;

    public float jump_force;
    private bool is_jumping;
    private bool on_ground;
    private float time_since_jump = 0;

    public PhysicMaterial default_physic_material;
    public PhysicMaterial jumping_physic_material; // we don't want to bounce when jumping
    
    public int skip_index = 0;
    public bool skipping = false;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        rb = GetComponent<Rigidbody>();
        cl = GetComponent<SphereCollider>();
        rb.maxAngularVelocity = 20;
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void OnJump(InputValue jump_button)
    {
        is_jumping = jump_button.isPressed;
    }

    void OnRestart(InputValue a)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void OnSkip(InputValue a)
    {
        skip_index += 1;
        IncreaseScore(-500);
        skipping = true;
    }

    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        
        // let the player come to a stop more easily when pressing nothing
        // this should not apply to aerial movement
        if (movement.magnitude > 0 || !on_ground) {
            rb.drag = 0f;
        } else {
            rb.drag = neutral_drag;
        }

        // gravity slows the player's movement too much when on ground, so we need to adjust for it
        // also, air control should be lessened anyway
        if (on_ground) {
            speed = floor_speed;
        } else {
            speed = air_speed;
        }

        // jumping disables bouncing, which would stack with jumping to allow unwanted height
        if (is_jumping) {
            cl.material = jumping_physic_material;
        } else {
            cl.material = default_physic_material;
        }
        // time_since_jump is necessary since on_ground doesn't stop activating quick enough
        if (is_jumping && on_ground && time_since_jump > 0.1) {
            rb.AddForce(Vector3.up * jump_force, ForceMode.Impulse);
            on_ground = false;
            time_since_jump = 0f;
        }

        rb.AddForce(movement * speed, ForceMode.Acceleration);
        on_ground = false;

        time_since_jump += Time.fixedDeltaTime;
    }

    private void OnCollisionStay(Collision other)
    {
        // don't jump off of walls/ceilings!
        for (int i = 0; i < other.contactCount; i++) {
            ContactPoint contact = other.GetContact(i);
            print(Vector3.Angle(contact.normal, Vector3.up));
            if (Vector3.Angle(contact.normal, Vector3.up) < 45) {
                on_ground = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PickUp"))
        {
            IncreaseScore(10);
        }

        if (other.CompareTag("SuperPickUp")) {
            IncreaseScore(200);
        }
    }

    public void IncreaseScore(int amount)
    {
        score += amount;
    }
}
