using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ConveyorBelt : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;
    private Renderer rd;
    
    public bool touched_yet = false;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rd = GetComponent<Renderer>();
    }


    private void Update()
    {
        Vector2 tex_offset = new Vector2(0f,  -Time.time * speed);
        rd.material.mainTextureOffset = tex_offset;
    }

    void FixedUpdate()
    {
        
        Vector3 disp = transform.forward * speed * Time.fixedDeltaTime;
        Vector3 pos = rb.position;
        rb.position -= disp;
        rb.MovePosition(pos);
        
    }

    private void OnCollisionStay(Collision other)
    {
        DoTheConveyor(other);
    }
    private void OnCollisionEnter(Collision other)
    {
        DoTheConveyor(other);
    }

    private void DoTheConveyor(Collision other)
    {
        other.rigidbody.MovePosition(other.transform.position + transform.forward * speed * Time.deltaTime);
        if (!touched_yet && other.gameObject.GetComponent<PlayerController>()) {
            other.gameObject.GetComponent<PlayerController>().IncreaseScore(5);
            touched_yet = true;
        }
    }
}
