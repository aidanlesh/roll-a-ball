using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{

    public PlayerController player;
    public int myIndex;

    // Update is called once per frame
    void Update()
    {
        if (player.skipping && player.skip_index == myIndex) {
            player.transform.position = transform.position;
            player.skipping = false;
        }
    }
}
