using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScoreCount : MonoBehaviour
{
    public PlayerController tracked_player;

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI winText;
    public int score_to_win;
    public float death_barrier_y;
    
    // Start is called before the first frame update
    void Start()
    {
        setScoreText();
        winText.gameObject.SetActive(false);
    }

    void setScoreText()
    {
        scoreText.text = "Score: " + tracked_player.score.ToString();
        if (tracked_player.transform.position.y <= death_barrier_y)
        {
            winText.gameObject.SetActive(true);
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        setScoreText();
    }
}
